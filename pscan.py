#!/usr/bin/env python
"""pscan - Detect processes that may cause load alarms or performance issues on a shared resource.

This module checks processes on a shared resource like the login node on an HPC cluster and takes appropriate
actions on those that may create a load alarm or cause performance issues.

Author: Xizhou Feng
Data: 08/15/2018
"""
import argparse
import subprocess
import re
import logging
import json
import sys
import pwd
import os
import socket
import string

sys.path.append(os.path.dirname(os.path.abspath(__file__)))
import templite

THIS_PROGRAM='pscan'
SeparatorRE = re.compile(r'\s+')
NumOfProcesses = 100

logger = logging.getLogger('pscan')
logging_level = logging.DEBUG


def init_logger(should_log_to_console=True, log_file_path=None):
    """Init the log facility.

    Args:
        should_log_to_console (bool): Whether write log to console. Default to True.
        log_file_path (string): The path to the log file. Default to None.
    """
    logger.setLevel(logging_level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s: %(message)s')
    if should_log_to_console:
        ch = logging.StreamHandler()
        ch.setLevel(logging_level)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    if log_file_path:
        fh = logging.FileHandler(log_file_path)
        fh.setFormatter(formatter)
        fh.setLevel(logging_level)
        logger.addHandler(fh)


def time_to_seconds(time_s):
    """Convert a time string to seconds

    Args:
        time_s (string): a time encoded in a string in the format of "[DD-]hh:mm:ss"

    Returns:
        int: total seconds derived from the time string
    """

    if '-' in time_s:
        dt = time_s.split("-")
        m = dt[1].split(':')
    else:
        m = time_s.split(':')

    seconds = 0
    for i in range(len(m)):
        seconds = seconds * 60 + float(m[i])
    if '-' in time_s:
        seconds += float(dt[0]) * 24 * 60 * 60
    return int(seconds)


class Process:
    configs = {}

    def __init__(self, pid, user, pcpu, cpu_time, pmem, rss, state, cmd, args):
        self.pid = pid
        self.user = user
        self.pcpu = pcpu
        self.cpu_time = cpu_time
        self.pmem = pmem
        self.rss = rss
        self.state = state
        self.cmd = cmd
        self.args = args
        self.fix_user()

    def fix_user(self):
        if self.user.endswith("+"):
            if Action.mode == 'debug':
                logger.debug("find long user name {}".format(self.user))
            uid_file = "/proc/{}/loginuid".format(self.pid)
            if os.path.exists(uid_file):
                f = open(uid_file, 'r')
                uid_s = f.readline()
                f.close()
                uid_s.rstrip()
                uid = int(uid_s)
                try:
                    self.user = pwd.getpwuid(uid).pw_name
                except KeyError:
                    logger.debug("could not recognize uid {} for user {}".format(uid, self.user))
                if Action.mode == 'debug':
                    logger.debug("fixed user {}".format(self.user))

    def __str__(self):
        return "pid={} user={} %cpu={} %mem={} time={}s rss={}MB state={} cmd={} args={}".format(
            self.pid, self.user, self.pcpu, self.pmem, self.cpu_time, self.rss, self.state, self.cmd, self.args)

    def match(self):
        if 'excluded_users' in Process.configs:
            if self.user in Process.configs['excluded_users']:
                if Action.mode == 'debug':
                    logger.debug("EXCLUDED: user {} is among excluded users".format(self.user))
                return False

        if 'excluded_programs' in Process.configs:
            if self.cmd in Process.configs['excluded_programs']:
                if Action.mode == 'debug':
                    logger.debug("EXCLUDED: progrm {} is among excluded programs".format(self.cmd))
                return False

        if 'filters' in Process.configs:
            if 'by_cpu_utilization' in Process.configs['filters']:
                process_filter = Process.configs['filters']['by_cpu_utilization']
                if self.pcpu > process_filter['min_pcpu_threshold']:
                    if self.cpu_time > process_filter['min_cpu_time']:
                        if Action.mode == 'debug':
                            logger.debug("MATHCH: matched cpu_threshod {} and cpu time {}".format(
                                process_filter['min_pcpu_threshold'],
                                process_filter['min_cpu_time']))
                        return True

            if 'by_command' in Process.configs['filters']:
                filtered_programs = Process.configs['filters']['by_command']
                for program in filtered_programs.keys():
                    if filtered_programs[program]:
                        cmd = self.cmd.split('/')[-1]
                        if cmd == program:
                            if Action.mode == 'debug':
                                logger.debug("MATCHED: program {} is among banned programs".format(program))
                            return True
        return False

    @staticmethod
    def scan():
        """Scan the processes and take actions on processes matching a set of criteria.

        """
        p = subprocess.Popen(['ps', 'aux', '--sort=-pcpu', '-w'], stdout=subprocess.PIPE)
        output = p.communicate()[0].decode().split('\n')
        for line in output[1: NumOfProcesses + 1]:
            m = SeparatorRE.split(line)
            if len(m) < 11:
                continue
            user, pid, pcpu, cpu_time = m[0], int(m[1]), float(m[2]), time_to_seconds(m[9])
            pmem, rss, state, cmd, arguments = float(m[3]), float(m[5]) / 1000, m[7], m[10], ' '.join(m[11:])
            process = Process(pid, user, pcpu, cpu_time, pmem, rss, state, cmd, arguments)
            if Action.mode == 'debug':
                print(process)
            if process.match():
                Process.take_action(process)

    @staticmethod
    def take_action(process):
        logger.info('take action on {}'.format(process))
        logger.info("actions = {}".format(Process.configs['actions']))
        if 'log' in Process.configs['actions']:
            logger.info("detected violating process: {}".format(process))

        if 'email' in Process.configs['actions']:
            if process.user not in Action.email_queue.keys():
                Action.email_queue[process.user] = []
            if Action.mode == 'debug':
                logger.debug('add process to email queue: {}'.format(process))
            Action.email_queue[process.user].append(str(process))

        if 'kill' in Process.configs['actions']:
            if Action.mode == 'debug':
                logger.debug('kill process {}'.format(process))
            Action.kill(process)

    @staticmethod
    def read_configuration(config_file):
        with open(config_file, "r") as cfg:
            Process.configs = json.load(cfg)


def search_user(user):
    """Find the user's full name

    Args:
        user(string): the username to be searched

    Returns:
        (username, first_name, last_name, email) if succeeds; [] otherwise
    """
    ldap_command = string.Template(Process.configs['ldap_command']).substitue(user)
    try:
        p = subprocess.Popen(ldap_command.split(), stdout=subprocess.PIPE)
        lines = p.communicate()[0].split('\n')
    except OSError:
        return []

    info = {}
    user_info = []
    for line in lines:
        items = re.split(':', line, maxsplit=1)
        if len(items) > 1:
            k, v = items[0], items[1]
            v = v.strip()
            info[k] = v
    if 'sn' in info.keys():
        if 'main' in info:
            email = info['mail']
        else:
            email = '{}@{}'.format(user, Process.configs['email_domain'])
        user_info = [user, info['givenName'], info['sn'], email]
    return user_info


def get_template(template_path):
    """get the template from the file specified by template_path

    Args:
        template_path(string):  path to the template file

    Returns:
        template(Template)
    """
    with open(template_path) as f:
        text = ''.join(f.readlines())
        template = templite.Templite(text)
    return template


class Action:
    subject = 'Process Scanner Notification'
    email_queue = {}

    @staticmethod
    def kill(process):
        cmd = ['kill', '-9', str(process.pid)]
        if Action.mode == 'police':
            subprocess.call(cmd)
            logger.info("{}".format(' '.join(cmd)))
        elif Action.mode == 'debug':
            logger.debug("{}".format(' '.join(cmd)))

    @staticmethod
    def notify_by_email(user_email, msg):
        from_email_addr = Process.configs['from_email']
        to_email_addr = user_email
        cmd = ['mailx', '-s', '"{}"'.format(Action.subject), '-r', from_email_addr, to_email_addr]
        if Action.mode == 'police':
            p = subprocess.Popen(cmd, stdin=subprocess.PIPE)
            p.communicate(input=msg)
            p.wait()
        logger.info('notifying user via email; cmd = {}'.format(' '.join(cmd)))

    @staticmethod
    def send_emails():
        user_email_domain = Process.configs['email_domain']
        admin_email = Process.configs['admin_email']
        hostname = socket.gethostname()

        template = Process.configs['email_template']
        for u in sorted(Action.email_queue.keys()):
            username, user_email = u, '{}@{}'.format(u, user_email_domain)
            if Process.configs['search_user_from_ldap']:
                user_info = search_user(u)
                if len(user_info) == 4:
                    username, user_email = user_info[1], user_info[3]

            data = {
                'username': username,
                'host': hostname,
                'process': Action.email_queue[u]
            }

            msg = template.render(data)
            if Action.mode == 'debug':
                print(msg)
                Action.notify_by_email(admin_email, msg)
            elif Action.mode == 'police':
                if 'users_with_bad_email' not in Process.configs:
                    Action.notify_by_email(user_email, msg)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Processes scan and police')
    parser.add_argument('-c', '--config', default='pscan.json', help='configuration file in json format')
    parser.add_argument('-m', '--mode', choices=['debug', 'police'], default='police', help='operation mode')

    args = parser.parse_args()

    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    Process.read_configuration(args.config)
    host = socket.gethostname()
    if 'host' in Process.configs and Process.configs['host'] != host:
        logger.error('This program can only run the host specified in the configure file "{}"'.format(args.config))
        sys.exit(1)

    Action.mode = args.mode
    if Action.mode == 'debug':
        log_to_console = True
    else:
        log_to_console = False

    if 'log_file' in Process.configs:
        init_logger(log_to_console, Process.configs['log_file'])
    else:
        init_logger(log_to_console, None)

    logger.info('start {} in {} mode.'.format(THIS_PROGRAM, Action.mode))
    Process.scan()
    Action.send_emails()
