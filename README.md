# pscan -- scan and kill matching processes

pscan is a python program that scans processes running on the login node and kills those
that match a set of criteria specified in a configuration file.

## Installation

1. Checkout this program into a desired directory (e.g.: `/root/bin`).
1. Edit the configuration file `process-scan.json` to match your site's requirements . 
1. Test the program to ensure it works as expected (command: `python /root/bin/pscan.py`).
1. Add a cron job to run this program periodically. As example is shown as follows.

      ```
      */3 * * * * /root/bin/pscan/pscan.py
      ```

## Main Idea

The pscan program incorporates two main ideas.

First, a practical process scanner program should be flexible when policing user processes. Thus, its behavior should 
change according to a configuration file. 

Second, when detecting some violating processes, the scanner should not just kill those processes. Instead, it should 
notify the user and educate the user how to work effectively on a shared compute resource.
   
## Implementation Details

### The Main Body

At its core, the pscan program simply goes through a list of processes, finds those that match a certain pattern, and 
then takes actions on them.
  
~~~python
processes = list_processes()
for p in processes:
    if match(p, criteria):
        take_action(p)
send_email()
~~~

### List Processes

The list_processes method is equivalent to the following command:

~~~bash
NUM_OF_PROCESSES=100
ps aux --sort=-pcpu -w | head -n $NUM_OF_PROCESSES
~~~

### Match Processes

~~~python
def match(p):
    if p.user in excluded_users:
        return False
    if p.cmd in excluded_programs:
        return False
    if p.pcpu > process_filters['by_cpu_utilization']['min_cpu_utilization']:
        if p.cpu_time > process_filters['by_cpu_utilization']['min_cpu_time']:
            return True
    if p.cmd in process_filters['by_command']:
        return True
~~~


### Take Action on Matching Process

~~~Python
define take_action(p):
    log.info(p)
    email_queue.append(p)
    kill(p)
~~~

## The Configuration File

The pscan program uses a json file (default to `pscan.json`) to specify its behavior. Changing the configuration values in any of the filters 
can impact the set of matching processes. 

All the configuration entries are easy to understand. When deploying this program on your site, you need to go through
this configuration file and make corresponding changes. 

Below is a sample configuration file. 

~~~json
{
    "actions": {
        "email": true, 
        "kill": true, 
        "log": true
    }, 
    "users_with_bad_email": ["nomail"],
    "excluded_programs": [
        "qsub", "sshd:", "vim"
    ], 
    "excluded_users": [
        "root",
        "delphi"
    ], 
    "filters": {
        "by_command": {
            "MATLAB": true,
            "matlab": true,
            "R": true,
            "mpirun": true,
            "namd2.11_multicore": false
        }, 
        "by_cpu_utilization": {
            "min_cpu_time": 300, 
            "min_pcpu_threshold": 50
        }
    },
    "email_template": "process-scan-email.tmpl",
    "host": "your_host",
    "log_file": "/var/log/process-scan.log",
    "email_domain": "your_domain.edu",
    "from_email": "myadmin@your_domain.edu",
    "admin_email": "myadmin@your_domain.edu",
    "search_user_from_ldap": false,
    "ldap_command": "ldapsearch -o ldif-wrap=no -x -D ou=users,ou=clemsonu uid=$user"
}
~~~

